# testTS

#### introduce
The function of this project is to pull the corresponding code and use cases, call the test machine to transform use cases, output results.
If it is consistent with handwritten use cases, output successful use cases and failed use cases, and set the failure path record to result.txt.

####  Architecture
testTs (Project folder) :
    test (remotely downloaded TS file)
    out (Use case storage for machine transformation)
    expect (storing your own written use cases)
    result.txt (the path where the results are successfully and failed)
    test_run.py (function entry)
    skip_test.json (save ts file path with problem skipped execution)
    config.py (Parameter configuration)
    utils.py (Base method call wrapper)

#### Instructions

1. Go to the testTs folder
2. Place use cases in an Expect folder
3. Run python3 run_testts.py
4. View the result

#### Expand the use

1.  Python3 run_testts.py --dir directory path (the folder to execute the corresponding path)
2.  Python3 run_testts.py --file File path (execute the file corresponding to the path)
3.  Python3 run_testts.py --ark-frontend-tool path (machine conversion tool selection)


#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request



