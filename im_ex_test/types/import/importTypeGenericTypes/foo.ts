// @filename: foo.ts
interface Point<T> {
    x: number;
    y: number;
    data: T;
}
export = Point;