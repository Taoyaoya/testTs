// @filename: foo.ts
interface Point {
    x: number;
    y: number;
}
export = Point;