// @noImplicitReferences: true
// @traceResolution: true
// @typeRoots: types


// @filename: /a.ts
import { x } from "./index";
import { y } from "./bop/index";
import { z } from "./z";
x;
y;
z;