// @Filename: ex.d.ts
// based on assert in @types/node
export function art(value: any, message?: string | Error): asserts value;