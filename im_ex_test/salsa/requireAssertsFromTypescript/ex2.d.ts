// @Filename: ex2.d.ts
declare function art(value: any, message?: string | Error): asserts value;
export = art;