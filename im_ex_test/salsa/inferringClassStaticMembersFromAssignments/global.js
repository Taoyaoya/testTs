//@filename: global.js
class C3 { }
C3.staticProp = 0;

function F3() { }
F3.staticProp = 0;

var C4 = class { };
C4.staticProp = 0;

let F4 = function () { };
F4.staticProp = 0;
