// @filename: a.js
export class C1 { }
C1.staticProp = 0;

export function F1() { }
F1.staticProp = 0;

export var C2 = class { };
C2.staticProp = 0;

export let F2 = function () { };
F2.staticProp = 0;

