// @allowJs: true
// @checkJs: true
// @noEmit: true

// @Filename: use.js
import * as s from './mod'

var k = new s.n.K()
k.x
var classic = new s.Classic()


/** @param {s.n.K} c
    @param {s.Classic} classic */
function f(c, classic) {
    c.x
    classic.p
}
