// @checkJs: true
// @allowJs: true
// @noEmit: true
// @Filename: test.js
// @strict: true
/** @typedef {{
    status: 'done'
    m(n: number): void
}} DoneStatus */



// @Filename: mod.js
// module.exports assignment
/** @type {{ status: 'done', m(n: number): void }} */
module.exports = {
    status: "done",
    m(n) { }
}
