// @allowJs: true
// @checkJs: true
// @strict: true
// @noEmit: true


// @Filename: use.js
var ex = require('./ex')

// values work
var crunch = new ex.Crunch(1);
crunch.n


// types work
/**
 * @param {ex.Crunch} wrap
 */
function f(wrap) {
    wrap.n
}
