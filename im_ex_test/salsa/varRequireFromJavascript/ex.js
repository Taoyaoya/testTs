// @Filename: ex.js
export class Crunch {
    /** @param {number} n */
    constructor(n) {
        this.n = n
    }
    m() {
        return this.n
    }
}