// @allowJs: true
// @strict: true
// @checkJs: true
// @emitDeclarationOnly: true
// @declaration: true


// @filename: mod2.js
const mod1 = require("./mod1");
mod1.a;
mod1.b;
mod1.c;
mod1.d;
mod1.d.e;
mod1.default;