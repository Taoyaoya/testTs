// @allowJs: true
// @checkJs: true
// @noEmit: true



// @Filename: use.js
/// <reference path='./types.d.ts'/>
/** @typedef {import("./mod1")} C
 * @type {C} */
var c;
c.chunk;

const D = require("./mod1");
/** @type {D} */
var d;
d.chunk;
