// @Filename: MW.js
/** @typedef {import("./MC")} MC */

class MW {
    /**
     * @param {MC} compiler the compiler
     */
    constructor(compiler) {
      this.compiler = compiler;
    }
  }
  
  module.exports = MW;