// @noEmit: true
// @allowjs: true
// @checkjs: true

// @Filename: MC.js
const MW = require("./MW");

/** @typedef {number} Cictema */

module.exports = class MC {
  watch() {
    return new MW(this);
  }
};
