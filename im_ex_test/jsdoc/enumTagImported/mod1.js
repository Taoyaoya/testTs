// @Filename: mod1.js

/** @enum {string} */
export const TestEnum = {
    ADD: 'add',
    REMOVE: 'remove'
}
