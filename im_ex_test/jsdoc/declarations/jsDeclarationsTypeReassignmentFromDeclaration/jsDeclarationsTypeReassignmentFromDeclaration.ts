// @allowJs: true
// @checkJs: true
// @outDir: /out
// @lib: es6
// @declaration: true

// @filename: index.js

/** @type {typeof import("/some-mod")} */
const items = [];
module.exports = items;