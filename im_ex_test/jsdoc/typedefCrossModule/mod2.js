
// @Filename: mod2.js
/// <reference path="./commonjs.d.ts"/>
/** @typedef {{ type: "a", x: 1 }} A */
/** @typedef {{ type: "b", y: 1 }} B */
/** @typedef {A | B} Both */

export function C() {
    this.p = 1
}
