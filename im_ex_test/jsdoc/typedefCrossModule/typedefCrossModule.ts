// @noEmit: true
// @allowJs: true
// @checkJs: true


// @Filename: use.js
/** @type {import('./mod1').Both} */
var both1 = { type: 'a', x: 1 };
/** @type {import('./mod2').Both} */
var both2 = both1;
/** @type {import('./mod3').Both} */
var both3 = both2;


