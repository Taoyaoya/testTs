// @noEmit: true
// @allowjs: true
// @checkjs: true

// @Filename: MC.js
const MW = require("./MW");

/** @typedef {number} Meyerhauser */

/** @class */
module.exports = function MC() {
    /** @type {any} */
    var x = {}
    return new MW(x);
};
