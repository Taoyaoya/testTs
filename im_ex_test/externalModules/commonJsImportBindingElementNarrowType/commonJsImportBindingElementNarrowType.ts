// Regresion test for GH#41957

// @allowJs: true
// @checkJs: true
// @strictNullChecks: true
// @noEmit: true



// @Filename: /bar.js
const { a } = require("./foo");
if (a) {
  var x = a + 1;
}