// @isolatedModules: true


// @Filename: /b.ts
export type { A } from './a'; // should not error, but would without `type`
