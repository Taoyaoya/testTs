// @Filename: foo2.ts
interface I1 {
	a: string;
	b: number;
}

class C1 {
	m1: I1;
}

export = C1; // Should fail, type I1 of visible member C1.m1 not exported.