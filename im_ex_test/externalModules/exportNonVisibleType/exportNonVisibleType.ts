// @Filename: foo3.ts
interface I1 {
	a: string;
	b: number;
}

class C1 {
	private m1: I1;
}

export = C1; // Should work, private type I1 of visible class C1 only used in private member m1.
