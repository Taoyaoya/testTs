// @Filename: foo1.ts
interface I1 {
	a: string;
	b: number;
}

var x: I1 = {a: "test", b: 42};
export = x; // Should fail, I1 not exported.