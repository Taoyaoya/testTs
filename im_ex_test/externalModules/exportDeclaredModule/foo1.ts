// @Filename: foo1.ts

declare module M1 {
	export var a: string; 
	export function b(): number;
}
export = M1;