// @Filename: declarations.d.ts
declare module "foo*baz" {
    export function foo(s: string): void;
}
// Augmentations still work
declare module "foo*baz" {
    export const baz: string;
}

// Longest prefix wins
declare module "foos*" {
    export const foos: string;
}

declare module "*!text" {
    const x: string;
    export default x;
}
