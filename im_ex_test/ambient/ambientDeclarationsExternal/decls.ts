//@Filename: decls.ts
// Ambient external module with export assignment
declare module 'equ' {
    var x;
    export = x;
}
declare module 'equ2' {
    var x: number;
}