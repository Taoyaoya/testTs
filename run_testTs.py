import os
import subprocess
import argparse
import datetime
import sys
import shutil
import json
from utils import *
from config import *

class MyException(Exception):  # 继承异常类
    def __init__(self, name):
        self.name = name

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir', metavar='DIR', help="Directory to test")
    parser.add_argument('--file', metavar='FILE', help="File to test")
    parser.add_argument('--ark_frontend_tool', help="ark frontend conversion tool")
    parser.add_argument('--case_type',default='class', help="Type of use case")

    arguments = parser.parse_args()
    return arguments


def skip(filepath):
    f = open(SKIP_FILE_PATH,'r')
    content = f.read()
    f.close()
    test_list = []
    skip_test = json.loads(content)
    skip_test_list = skip_test['error.txt'] + skip_test['no2015'] + skip_test['tsc_error']
    if os.path.isfile(filepath):
        if filepath not in skip_test_list:
            return True
        else:
            return False
    if os.path.isdir(filepath):
        for root,dirs,files in os.walk(filepath):
            for file in files:
                test_path = f'{root}/{file}'
                if test_path not in skip_test_list:
                    if '.ts' in test_path:
                        test_list.append(test_path)
        return test_list    

def abc_judge(filepath):
    if not os.path.getsize(filepath):
        print(f'Error : {filepath} 文件为空')

#使用机器转换（path：ts文件路径，out_file：输出内容写入文件路径,tools:工具选择，默认为'ohos-arm-release/clang_x64/ark/ark/build/src/index.js'）
def run_test(file, tool,args):
    path_list = file.split(os.sep)
    if path_list[0] != '.':
        file = "." + os.sep + file
    out_file_path = file.replace(TEST_PATH,OUT_PATH).replace(TS_EXT,TXT_EXT)
    temp_out_file_path = file.replace(TS_EXT,TXT_EXT)
    temp_abc_file_path = file.replace(TS_EXT,ABC_EXT)
    path_list = out_file_path.split(os.sep)
    path_list.pop(-1)
    out_dir_path = os.sep.join(path_list)
    if not os.path.exists(out_dir_path):
        os.makedirs(out_dir_path)
    if args.case_type == 'import':
        try:
            command_os(f'node --expose-gc {tool} -m {file}  --output-type -g')
        except:
            e = str(e)
            write_file(out_file,e)
    elif args.case_type == 'class':
        try:
            # print(f'node --expose-gc {tool} -m {file}  --output-type')
            command_os(f'node --expose-gc {tool} -m {file}  --output-type -g')
        except:
            e = str(e)
            write_file(out_file,e) 
        
    if os.path.exists(temp_abc_file_path):
        abc_judge(temp_abc_file_path)
        remove_file(temp_abc_file_path)
    if os.path.exists(temp_out_file_path):
        move_file(temp_out_file_path, out_file_path)

def run_test_machine(args):
    ark_frontend_tool = DEFAULT_ARK_FRONTEND_TOOL
    if args.ark_frontend_tool:
        ark_frontend_tool = args.ark_frontend_tool

    if args.file:
        if skip(args.file):
            run_test(args.file, ark_frontend_tool,args)
    elif args.dir:
        test_list = skip(args.dir)
        for file in test_list:
            run_test(file, ark_frontend_tool,args)
    elif args.file == None and args.dir == None:
        test_list = skip(TS_CASES_DIR)
        for file in test_list:
            run_test(file, ark_frontend_tool,args)
            
def run_test_split(file, tool,args):
    path_list = file.split(os.sep)
    if path_list[0] != '.':
        file = "." + os.sep + file
    out_file_path = file.replace(TEST_PATH,OUT_PATH).replace(TS_EXT,TXT_EXT)
    temp_out_file_path = file.replace(TS_EXT,TXT_EXT)
    ts_list = temp_out_file_path.split(os.sep)
    ts_list.pop(-1)
    ts_dir_path = os.sep.join(ts_list)
    path_list = out_file_path.split(os.sep)
    path_list.pop(-1)
    out_dir_path = os.sep.join(path_list)   
    if not os.path.exists(out_dir_path):
        os.makedirs(out_dir_path)
    try:
        command_os(f'node --expose-gc {tool} -m {file}  --output-type')
    except:
        e = str(e)
        write_file(out_file,e)
        
    for root,dirs,files in os.walk(ts_dir_path):
        for fi in files:  
            ts_file =  f'{root}/{fi}'     
            if ABC_EXT in ts_file:
                remove_file(ts_file)
            elif TXT_EXT in ts_file:
                sj_path =ts_file.replace(TEST_PATH,OUT_PATH)  
                move_file(ts_file,sj_path) 


def run_test_machine_split(args):
    ark_frontend_tool = DEFAULT_ARK_FRONTEND_TOOL
    if args.ark_frontend_tool:
        ark_frontend_tool = args.ark_frontend_tool

    if args.file:
        if skip(args.file):
            run_test_split(args.file, ark_frontend_tool,args)
            
    elif args.dir:
        test_list = skip(args.dir)
        for file in test_list:
            run_test_split(file, ark_frontend_tool,args)
    elif args.file == None and args.dir == None:
        test_list = skip(TS_CASES_DIR)
        for file in test_list:
            run_test_split(file, ark_frontend_tool,args)
    
    
def read_out_file(file_path):
    f = open(file_path,'r')
    content = f.read()
    f.close()
    if content:
        if '}\n{' in content:
            a = content.split('}\n{')
        else:
            a =[]
            a.append(''.join(content.split('\n')))
    else:
        a = []
    li = []
    if len(a) > 1:
        for i in range(len(a)):
            if i == 0:
                b = ''.join(a[i].split('\n')).strip(' ') + '}'
            elif i == (len(a)-1):
                b = '{' + ''.join(a[i].split('\n')).strip(' ')
            else:
                b = ('{' + ''.join(a[i].split('\n')).strip(' ') + '}')
            c = json.loads(b)
            li.append(c)
    else:
        for i in range(len(a)):
            c = json.loads(a[i])
            li.append(c)
    return li

def compare(file):
    result = ""
    path_list = file.split(os.sep)
    if path_list[0] != '.':
        file = "." + os.sep + file
    out_path = file.replace(TEST_PATH, OUT_PATH).replace(TS_EXT,TXT_EXT)
    expect_path = file.replace(TEST_PATH, EXPECT_PATH).replace(TS_EXT,TXT_EXT)
    if (not os.path.exists(out_path) or not os.path.exists(expect_path)):
        print("There are no expected files or validation file generation: %s", file)
        result = f'FAIL {file}\n'
    else:
        outcont = read_out_file(out_path)
        expectcont = read_file(expect_path)
        expectcontlist = []
        for i in expectcont:
            i = json.loads(i.replace("'",'"'))
            expectcontlist.append(i)
        if outcont == expectcontlist:
            result = f'PASS {file}\n'
        else:
            result = f'FAIL {file}\n'
    for i in outcont:
        print(i)
    return result
  
def read_compare(args):
    success = 0
    fail = 0
    result_path =[]
    if args.file:
        result = compare(args.file)
        result_path.append(result)
    elif args.dir:
        test_dir_path = skip(args.dir)
        for file in test_dir_path:
            result = compare(file)
            result_path.append(result)
    elif args.file == None and args.dir == None:
        test_dir_path = skip(args.dir)
        for file in test_dir_path:
            result = compare(file)
            result_path.append(result)
    f = open(OUT_RESULT_FILE,'w')
    f.writelines(result_path)
    f.close()

def compare_split(file):
    result = ""
    path_list = file.split(os.sep)
    if path_list[0] != '.':
        file = "." + os.sep + file
    out_path = file.replace(TEST_PATH, OUT_PATH).replace(TS_EXT,TXT_EXT)
    out_path_dir = out_path.split(os.sep)
    out_path_dir.pop(-1)
    out_path_txt = os.sep.join(out_path_dir)
    # print(out_path)
    out_list = []
    expect_list =[]
    for root,dirs,files in os.walk(out_path_txt):
        for fi in files:
            out_txt = f'{root}/{fi}'
            expect_txt = out_txt.replace(OUT_PATH,EXPECT_PATH)
            out_list.append(out_txt)
            expect_list.append(expect_txt)

    count = 0
    for num in range(len(out_list)):
        if not os.path.exists(expect_list[num]):
            print("There are no expected files or validation file generation: %s", {file})
            result = f'FAIL {file}\n'
            break
        outcont = read_out_file(out_list[num])
        expectcont = read_file(expect_list[num])
        expectcontlist = []
        # print(outcont)
        for i in expectcont:          
                i = json.loads(i.replace("'",'"'))
                expectcontlist.append(i)
        # print(expectcontlist)
        if outcont == expectcontlist:
            count += 1
    if count == len(out_list):
        if not os.path.exists(out_path):
            print("There are no expected files or validation file generation: %s", {file})
            result = f'FAIL {file}\n'
        else:
            result = result = f'PASS {file}\n'
    return result

def read_compare_split(args):
    success = 0
    fail = 0
    result_path =[]
    if args.file:
        result = compare_split(args.file)
        result_path.append(result)
    elif args.dir:
        test_dir_path = skip(args.dir)
        for file in test_dir_path:
            result = compare_split(file)
            result_path.append(result)
    elif args.file == None and args.dir == None:
        test_dir_path = skip(args.dir)
        for file in test_dir_path:
            result = compare_split(file)
            result_path.append(result)
    f = open(OUT_RESULT_FILE,'w')
    f.writelines(result_path)
    f.close()

def summary():
    if not os.path.exists(OUT_RESULT_FILE):
        return
    count = -1
    fail_count = 0
    for count, line in enumerate(open(OUT_RESULT_FILE, 'r')):
        if line.startswith("FAIL"):
            fail_count += 1
        pass
    count += 1

    print("\n      Regression summary")
    print("===============================")
    print("     Total         %5d         " %(count))
    print("-------------------------------")
    print("     Passed tests: %5d         " %(count-fail_count))
    print("     Failed tests: %5d         " %(fail_count))
    print("===============================")


def init_path():
    remove_dir(OUT_TEST_DIR)
    mk_dir(OUT_TEST_DIR)

    

#拉取用例和ts测试文件
def prepare_ts_code():
    if (os.path.exists(TS_CASES_DIR)):
        return
    try:
        mk_dir(TS_CASES_DIR)
        os.chdir('./testTs/test')
        command_os('git init')
        command_os(f'git remote add origin {TS_GIT_PATH}')
        command_os('git config core.sparsecheckout true')
        command_os('echo "tests/cases/conformance/" >> .git/info/sparse-checkout')
        command_os(f'git pull --depth 1 origin {TS_TAG}')
        command_os('rm -rf .git')
        if not os.path.exists("./tests/cases/conformance/"):
            remove_dir(TS_CASES_DIR)
            raise MyException("Pull TypeScript Code Fail, Please Check The Network Request")
        command_os(f'cp -r tests/cases/conformance/*   ./')
        command_os(f'rm -rf ./tests')
        os.chdir('../../')
    except:
        print("pull test code fail")

def main(args):
    if args.case_type == 'import':
        try:
            init_path()
            prepare_ts_code()
            run_test_machine_split(args)
            read_compare_split(args)
            summary()
        except:
            print("Run Python Script Fail")
    else:
        try:
            init_path()
            # excuting_npm_install(args)
            prepare_ts_code()
            run_test_machine(args)
            read_compare(args)
            summary()
        except:
            print("Run Python Script Fail")
    
if __name__ == "__main__":
    sys.exit(main(parse_args()))