module A {
    export class Point {
        x: number;
        y: number;
    }
    let pp = new Point();
}

module A {
    class Point {
        fromCarthesian(p: A.Point) {
            return { x: p.x, y: p.y };
        }
    }
    let pp1 = new Point();
}

// ensure merges as expected
var p: { x: number; y: number; };
var p: A.Point;

module X.Y.Z {
    export class Line {
        length: number;
    }
    let ll = new Line();
}

module X {
    export module Y {
        export module Z {
            class Line {
                name: string;
            }
            let ll1 = new Line()
        }
    }
}

// ensure merges as expected
var l: { length: number; }
var l: X.Y.Z.Line;

