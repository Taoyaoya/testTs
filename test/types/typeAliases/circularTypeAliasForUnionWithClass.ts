var v0: T0;
type T0 = string | I0;
class I0 {
    x: T0;
}

var v3: T3;
type T3 = string | I3;
class I3 {
    [x: number]: T3;
}

var v4: T4;
type T4 = string | I4;
class I4 {
    [x: string]: T4;
}

let i0 = new I0();
let i3 = new I3();
let i4 = new I4();