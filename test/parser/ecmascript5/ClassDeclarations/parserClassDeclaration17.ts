// @lib: es5
declare class Enumerator {
    public atEnd(): boolean;
    public moveNext();
    public item(): any;
    constructor (o: any);
}
let q22 = new Enumerator();